var menu = argument0;

var menuItemCount = ds_list_size(menu.menuItems);
for(var i=0; i<menuItemCount; i++)
{
    scr_MenuItemClean(menu.menuItems[| i]);
}

menu.isActive = false;
