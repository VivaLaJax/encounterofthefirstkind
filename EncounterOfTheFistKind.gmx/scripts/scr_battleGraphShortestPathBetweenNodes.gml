var battleGraph = instance_find(obj_battleGraph,0);
var nodeA = argument0;
var nodeB = argument1;
var nodesVisited = argument2;
var path = argument3;

var shortestPathFound = ds_list_create();
var currentPathFound = ds_list_create();

if(ds_list_find_index(nodesVisited,nodeA)!=-1)
{
    return path;
}
ds_list_add(nodesVisited,nodeA);

var connectedNodesFromA = battleGraph.connectionsMap[? nodeA];
var nodesFromACount = ds_list_size(connectedNodesFromA);

if(nodesFromACount==0)
{
    return path;
}

for(var i=0; i<nodesFromACount; i++)
{
    if(connectedNodesFromA[| i]==nodeB)
    {
        ds_list_add(path,nodeB);
        ds_list_insert(path,0,nodeA);
        return path;
    }
}

for(var i=0; i<nodesFromACount; i++)
{
    currentPathFound = scr_battleGraphShortestPathBetweenNodes(connectedNodesFromA[| i],nodeB,nodesVisited,currentPathFound);
    if(ds_list_size(currentPathFound)>0)
    {
        ds_list_insert(currentPathFound,0,nodeA);
        if(ds_list_size(currentPathFound)<ds_list_size(shortestPathFound) || ds_list_size(shortestPathFound)==0 )
        {
            shortestPathFound = currentPathFound;
        }
    }
}

ds_list_copy(path,shortestPathFound);
return path;
