//if ready for next action
if(readyForNextEntry)
{
    show_debug_message("Action handler entryIndex: "+string(currentEntryIndex)+" of entryCount: "+string(entryCount));
    if(ds_list_size(completedEntries)==0)
    {  
        show_debug_message("action handler complete");
        scr_ActionHandlerDeleteEntries(id);
        scr_battleMasterOnActionCompleted(actionSource);
        instance_destroy();
    }
    else
    {
        if(scr_ActionHandlerNextEntryIsAnimation(id))
        {
          //if not
//take the entry index and find the animation or action
//if it's an animation
//play the animation on the source, pass this id to be called back when it's complete
            show_debug_message("animation running");
            scr_ActionHandlerProcessNextAnimation(id);
        }
        else if(scr_ActionHandlerNextEntryIsAction(id))
        {
        //if it's an action
//run the script and pass it the parameter map with this id to call ready for next
            show_debug_message("action running");
            scr_ActionHandlerProcessNextAction(id);
        }
    }
    readyForNextEntry = false;
}
