var battleMaster = instance_find(obj_battleMaster,0);

var deadEnemyCount = 0;
var enemies = battleMaster.enemyCharacters;
var enemyCount = array_length_1d(enemies);
for(var i=0; i<enemyCount; i++)
{
    if(enemies[i].isDead)
    {
        deadEnemyCount++;
    }
}

if(enemyCount==deadEnemyCount)
{
    return true;
}

return false;
