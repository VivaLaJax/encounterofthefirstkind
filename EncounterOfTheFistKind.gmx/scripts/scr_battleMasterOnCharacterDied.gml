var target = argument0;
var battleMaster = instance_find(obj_battleMaster,0);

show_debug_message("Target " +string(target.name)+" has died!");
target.isDead = true;

var node = scr_battleGraphGetGraphNodeByObject(target);
node.objectContained = 0;
ds_list_delete(battleMaster.turnOrder, ds_list_find_index(battleMaster.turnOrder,target));
