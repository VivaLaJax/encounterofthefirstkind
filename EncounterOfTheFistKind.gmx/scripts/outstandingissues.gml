/**
Outstanding Bugs
- The game leaks a shit tonne of memory? (won't fix)
- The enemy path finding doesn't work as expected (travels 6 nodes?) (wont fix)
- Flash effect runs on whole screen (wont fix)
- Not passing inputs to shader (wont fix)
- Need different background music
- Can end up with infinite turns somehow?

To-Do
- Spit ball for enemy doesn't point towards it (won't fix)
- Positioning for chars when move to target? scr_getNonIntersectingSpritePosAtTarget (wont fix)
- Reduce number of orientations to 4 (wont fix)


When a character attacks:
Play attack animation ANIM
Deal Damage           ACTION
Play effect animations ANIM
Play death/hurt animation ANIM
Play Idle animation ANIM
End Action

When a character moves:
Play moving animation ANIM
Move to position ACTION
Play Idle animation ANIM
End Action
**/


