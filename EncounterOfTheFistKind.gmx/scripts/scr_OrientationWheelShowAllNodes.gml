var orientationWheel = argument0;
var show = argument1;

for(var i=0; i<orientationWheel.numDirections; i++)
{
    orientationWheel.directionNodes[| i].hidden = !show;
}
