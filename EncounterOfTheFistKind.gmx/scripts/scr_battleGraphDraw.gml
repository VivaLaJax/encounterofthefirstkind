var nodeRadius = 10;

//for each point, draw an oval where the node is
//draw a line for each connection
var drawnConnections = ds_list_create();
var nodeCount = array_length_1d(nodes);
for(var i=0; i < nodeCount; i++)
{
    var node = nodes[i];
    var connectedNodes = connectionsMap[? nodes[i]];
    var connectedNodesCount = ds_list_size(connectedNodes);
    for(var j=0; j<connectedNodesCount; j++)
    {
        var connection = ds_map_create();
        connection[? connectedNodes[| j]] = node;
        if(ds_list_find_index(drawnConnections, connection)==-1)
        {
            draw_line_width_colour(node.x, node.y, connectedNodes[| j].x, connectedNodes[| j].y, nodeRadius-1,colour_chill_orange ,colour_chill_orange);
            var myConnection = ds_map_create();
            myConnection[? node] = connectedNodes[| j];
            ds_list_add(drawnConnections, myConnection);
        }
    }
}

for(var i=0; i<nodeCount; i++)
{
    var node = nodes[i];
    var nodeColour = c_white;
    if(node.highlighted)
    {
        nodeColour = c_yellow;
    }
    draw_circle_colour(node.x,node.y,nodeRadius,nodeColour,nodeColour, false);
    draw_circle(node.x,node.y,nodeRadius, true);
}
