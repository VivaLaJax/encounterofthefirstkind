if(!hidden)
{
    var colour = c_white;
    if(highlighted)
    {
        colour = c_yellow;
    }
    
    draw_circle_colour(x,y,radius,colour,colour,false);
    draw_circle(x,y,radius,true);
}
