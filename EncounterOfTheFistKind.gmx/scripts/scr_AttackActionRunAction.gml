if(runAction)
{
    show_debug_message("Attack action is running!");

    var actionHandler = instance_create(x,y,obj_ActionHandler);
    actionHandler.actionSource = id;
    
    var originalX = source.x;
    var originalY = source.y;
    
    var travelParametersMap = ds_map_create();
    scr_getNonIntersectingSpritePosAtTarget(travelParametersMap,source,target);
    ds_map_add(travelParametersMap,"source",source);
    ds_map_add(travelParametersMap,"time",500);
    ds_map_add(travelParametersMap,"sfx",source.runSound);
    
    scr_ActionHandlerAddAnimationEntry(actionHandler,source, "Run", ANIMATION_REPEAT);
    scr_ActionHandlerAddActionEntry(actionHandler,scr_TravelToPosition,travelParametersMap,"TravelToPos");
    
    scr_ActionHandlerAddAnimationEntry(actionHandler,source, "Attack", ANIMATION_PLAY_ONCE);

    var damage = 8*(source.strength-target.defense);
    var parametersMap = ds_map_create();
    ds_map_add(parametersMap,"target",target);
    ds_map_add(parametersMap,"damage",damage);
    ds_map_add(parametersMap,"orientation",source.orientation);
    scr_ActionHandlerAddActionEntry(actionHandler,scr_DealDamage,parametersMap,"DealDamage");
    //deal damage should add effect and death/hurt animations
    
    var travelBackParametersMap = ds_map_create();
    ds_map_add(travelBackParametersMap,"source",source);
    ds_map_add(travelBackParametersMap,"targetX",originalX);
    ds_map_add(travelBackParametersMap,"targetY",originalY);
    ds_map_add(travelBackParametersMap,"time",500);
    
    scr_ActionHandlerAddAnimationEntry(actionHandler,source, "Run", ANIMATION_REPEAT);
    scr_ActionHandlerAddActionEntry(actionHandler,scr_TravelToPosition,travelBackParametersMap,"TravelToPos");

    scr_ActionHandlerAddAnimationEntry(actionHandler,source, "Idle", ANIMATION_REPEAT);
    
    scr_ActionHandlerStart(actionHandler);
        
    runAction = false;
}

//probably out to only be able to attack
//if orientation matches connection direction
//or change it as you attack?

//should change target direction to face source?
