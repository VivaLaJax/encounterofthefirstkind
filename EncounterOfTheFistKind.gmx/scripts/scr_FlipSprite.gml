var gameObj = argument0;

if(gameObj.spriteDirection == SPRITE_DIRECTION_LEFT)
{
    gameObj.spriteDirection = SPRITE_DIRECTION_RIGHT;
}
else
{
    gameObj.spriteDirection = SPRITE_DIRECTION_LEFT;
}

gameObj.image_xscale = gameObj.image_xscale * gameObj.spriteDirection;
