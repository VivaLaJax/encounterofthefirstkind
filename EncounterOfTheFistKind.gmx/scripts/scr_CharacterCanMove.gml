//A character can move if there is an open node within range
var character = argument0;

var homeNode = scr_battleGraphGetGraphNodeByObject(character);
if(homeNode==-1)
{
    return false;
}
var possibleTargetNodes = scr_battleGraphGetNodesWithinRange(homeNode, character.movementRange);
if(ds_list_size(possibleTargetNodes)>0)
{
    return true;
}

return false;
