var paramsMap = argument0;
var actionEntry = argument1;

var source = paramsMap[? "source"];

var projectileObj = instance_create(source.x,source.y-source.sprite_height/2,paramsMap[? "projectile"]);
show_debug_message(string(actionEntry)+" is action entry for projectile obj " + string(projectileObj));
var target = paramsMap[? "target"];
projectileObj.targetX = target.x;
projectileObj.targetY = target.y-target.sprite_height/2;
projectileObj.fired = true;
projectileObj.timeToTravel = paramsMap[? "timeToReach"];
projectileObj.actionEntry = actionEntry;
