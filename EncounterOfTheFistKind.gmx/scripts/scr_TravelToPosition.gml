var paramsMap = argument0;
var actionEntry = argument1;

var travelObj = instance_create(x,y,obj_TravelToPosition);
show_debug_message(string(actionEntry)+" is action entry for travel obj " + string(travelObj));
scr_TravelToPositionSetParameters(travelObj,paramsMap, actionEntry);
