if(alive<aliveMax)
{
    view_xview = random_range(shakeValueLower,shakeValueUpper);
    view_yview = random_range(shakeValueLower,shakeValueUpper);
    alive++;
}
else
{
    view_xview = 0;
    view_yview = 0;
    instance_destroy();
}
