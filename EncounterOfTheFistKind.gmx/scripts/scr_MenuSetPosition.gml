var menu = argument0;
var x_pos = argument1;
var y_pos = argument2;

menu.x = x_pos;
menu.y = y_pos;

var menuItemCount = ds_list_size(menu.menuItems);
var previousItemYBottom = y_pos;
for(var i=0; i<menuItemCount; i++)
{
    menu.menuItems[| i].x = x_pos;
    menu.menuItems[| i].y = previousItemYBottom;
    previousItemYBottom = menu.menuItems[| i].y + menu.menuItems[| i].height;
}
