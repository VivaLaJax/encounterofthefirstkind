if(hide)
{
    return 0;
}

var buffer = 3;
var textYBase = (height/4)-buffer;

var colour = c_dkgray;
    
if(highlighted)
{
    colour = c_gray;
}
               
draw_rectangle_colour(x,
                      y,
                      x+width,
                      y+height,
                      colour,
                      colour,
                      colour,
                      colour,
                      false);
                      
draw_rectangle(x,
               y,
               x+width,
               y+height,
               true);
                      
var name = action.name;
draw_text(x+buffer, textYBase+y, name);
