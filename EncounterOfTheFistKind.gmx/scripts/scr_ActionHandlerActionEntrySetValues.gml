var actionEntry = argument0;
var actionScript = argument1;
var parameterMap = argument2;
var handler = argument3;
var actionName = argument4;

actionEntry.actionScript = actionScript;
actionEntry.parameterMap = parameterMap;
actionEntry.entryIndex = handler.entryIndex;
actionEntry.actionHandler = handler;
actionEntry.actionName = actionName;
