var battleMaster = instance_find(obj_battleMaster,0);

if(debugInput and keyboard_check_released(ord('M')))
{
    scr_SoundMasterToggleVolume();
}

if(keyboard_check_released(ord('J')))
{
    scr_battleMasterOnInputReceived(battleMaster,INPUT_ENTER);
    return 0;
}

if(keyboard_check_released(ord('K')))
{
    scr_battleMasterOnInputReceived(battleMaster,INPUT_CANCEL); 
    return 0;   
}

var up = false;
var down = false;
var input = -1;

if(keyboard_check_released(ord('W')))
{
    up = true;
    input = INPUT_UP;
}

if(keyboard_check_released(ord('S')))
{
    down = true;
    input = INPUT_DOWN;
}

if(keyboard_check_released(ord('A')))
{
    if(up)
    {
        input = INPUT_NW;
    }
    else if(down)
    {
        input = INPUT_SW;
    }
    else
    {
        input = INPUT_LEFT;
    }
}

if(keyboard_check_released(ord('D')))
{
    if(up)
    {
        input = INPUT_NE;
    }
    else if(down)
    {
        input = INPUT_SE;
    }
    else
    {
        input = INPUT_RIGHT;
    }
}

if(input!=-1)
{
    scr_battleMasterOnInputReceived(battleMaster,input);
}
