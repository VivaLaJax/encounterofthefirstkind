animations = ds_list_create();
animationStartMap = ds_map_create();
animationEndMap = ds_map_create();
animationSpriteMap = ds_map_create();
animationSpeedMap = ds_map_create();
currentAnimation = 0;
currentSpriteIndex = 0;
playMode = ANIMATION_STOPPED;
animationComplete = false;
animationRequestIndex = 0;
