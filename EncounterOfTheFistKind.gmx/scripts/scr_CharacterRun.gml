if(playMode==ANIMATION_STOPPED)
{
    scr_AnimationControllerPlayAnimation(id,"Idle",ANIMATION_REPEAT);
}

if(previousRunningShader!=runningShader)
{
    if(runningShader==noone)
    {
        shader_reset();
    }
    else
    {
        shader_set(runningShader);
    }
    
    previousRunningShader = runningShader;
}

if(isDead)
{
    if(!hasPlayedDeathSound)
    {
        scr_SoundMasterPlaySound(deadSound);
    }
}
