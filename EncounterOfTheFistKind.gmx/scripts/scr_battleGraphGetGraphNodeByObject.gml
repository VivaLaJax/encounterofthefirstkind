var battleGraph = instance_find(obj_battleGraph,0);
var objectToFind = argument0;

var nodesLength = array_length_1d(battleGraph.nodes);
for(var i=0; i<nodesLength; i++)
{
    if(battleGraph.nodes[i].objectContained==objectToFind)
    {
        return battleGraph.nodes[i];
    }
}

return -1;
