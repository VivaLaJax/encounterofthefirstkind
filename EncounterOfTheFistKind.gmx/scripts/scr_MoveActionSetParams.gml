var moveAction = argument0;
var name = argument1;
var source = argument2;
var range = argument3;

moveAction.name = name;
moveAction.source = source;
moveAction.range = range;

var sourceNode = scr_battleGraphGetGraphNodeByObject(source);
possibleTargetNodes = scr_battleGraphGetNodesWithinRange(sourceNode, moveAction.range);
scr_SelectTargetActionSetParams(moveAction.targetSelectionAction,possibleTargetNodes,moveAction);
