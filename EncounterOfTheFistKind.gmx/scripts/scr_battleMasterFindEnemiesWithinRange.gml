var battleMaster = instance_find(obj_battleMaster,0);
var nodeA = argument0;
var range = argument1;

var enemies = ds_list_create();
var enemyNumber = array_length_1d(battleMaster.enemyCharacters);
show_debug_message("Enemy Num: " + string(enemyNumber));
for(var i=0; i< enemyNumber; i++)
{
    if(battleMaster.enemyCharacters[i].isDead)
    {
        continue;
    }
    var nodeB = scr_battleGraphGetGraphNodeByObject(battleMaster.enemyCharacters[i]);
    if(nodeB==-1 )
    {
        continue;
    }
    var distance = scr_battleGraphNodeDistance(nodeA, nodeB,ds_list_create());
    if(distance!=-1 && distance<=range)
    {
        ds_list_add(enemies, battleMaster.enemyCharacters[i]);
    }
}

return enemies;

