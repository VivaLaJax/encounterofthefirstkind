if(!hasPlayedSpedMusic)
{
    var player = instance_find(obj_Player, 0);
    playerParty = player.party;
    
    var allRedHealth = true;
    
    var partyNumber = ds_list_size(playerParty);
    for(var i=0; i<partyNumber; i++)
    {
        var percent = playerParty[| i].currentHealth/playerParty[| i].maxHealth;
        if(percent>=0.3)
        {
            allRedHealth = false;
        }
    }
    
    if(allRedHealth)
    {
        scr_SoundMasterPlayMusic(bgm_BackgroundMusicSped);
        hasPlayedSpedMusic = true;
    }
}
