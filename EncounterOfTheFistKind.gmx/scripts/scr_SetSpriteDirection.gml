var gameObj = argument0;
var spriteDirection = argument1;

if(gameObj.spriteDirection != spriteDirection)
{
    gameObj.spriteDirection = spriteDirection;
    gameObj.image_xscale = gameObj.image_xscale * -1;
}
