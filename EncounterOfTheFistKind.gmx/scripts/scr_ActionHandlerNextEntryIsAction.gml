var actionHandler = argument0;

var index = actionHandler.entryOrder[| actionHandler.currentEntryIndex];
var count = ds_list_size(actionHandler.actionEntries);
for(var i=0; i<count; i++)
{
    if(actionHandler.actionEntries[| i].entryIndex==index)
    {
        show_debug_message("selected action index = "+string(actionHandler.actionEntries[| i].entryIndex)+". Entry index= "+string(index));
        return true;
    }
}

return false;
