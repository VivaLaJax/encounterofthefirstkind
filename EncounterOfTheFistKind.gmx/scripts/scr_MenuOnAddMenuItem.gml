var menu = argument0;
var menuItem = argument1;

ds_list_add(menu.menuItems, menuItem);
var menuItemCount = ds_list_size(menu.menuItems);
var latestItemIndex = menuItemCount-1;

if(menuItemCount==1)
{
    menu.menuItems[| latestItemIndex].highlighted = true;
}

if(menu.menuItems[| latestItemIndex].width>menu.menuWidth)
{
    menu.menuWidth = menu.menuItems[| latestItemIndex].width;
}

var previousMenuHeight = menu.menuHeight;
menu.menuHeight = menu.menuHeight + menu.menuItems[| latestItemIndex].height;

menu.menuItems[| latestItemIndex].x = menu.x;
menu.menuItems[| latestItemIndex].y = menu.y + previousMenuHeight;
