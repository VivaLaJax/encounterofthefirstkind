var actionHandler = argument0;

var entry = noone;
var index = actionHandler.entryOrder[| actionHandler.currentEntryIndex];
var count = ds_list_size(actionHandler.animationEntries);
for(var i=0; i<count; i++)
{
    if(actionHandler.animationEntries[| i].entryIndex==index)
    {
        entry = actionHandler.animationEntries[| i];
        break;
    }
}

entry.runAnimation = true;
