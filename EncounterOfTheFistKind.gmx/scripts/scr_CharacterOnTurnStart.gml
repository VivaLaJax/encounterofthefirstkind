var character = argument0;

show_debug_message("Character " +string(character.name)+" is taking a turn!");

var menu = instance_create(0,0,obj_Menu);

if(scr_CharacterCanAttack(character))
{
    var homeNode = scr_battleGraphGetGraphNodeByObject(character);
    if(homeNode!=-1)
    {
        var enemiesWithinRange = scr_battleMasterFindEnemiesWithinRange(homeNode, character.attackRange);
        var enemiesCount = ds_list_size(enemiesWithinRange);
        if(enemiesCount!=0)
        {
            var attackSubMenu = instance_create(0,0,obj_Menu);
            for(var i=0; i<enemiesCount; i++)
            {
                //would be easier to have an attack action with a target action instead for ui?
                var attackAction = instance_create(0,0,obj_AttackAction);
                scr_AttackActionSetParams(attackAction,enemiesWithinRange[| i].name,enemiesWithinRange[| i],character);
                
                var attackMenuItem = instance_create(0,0,obj_MenuItem);
                scr_MenuItemSetAction(attackMenuItem, attackAction);
                scr_MenuOnAddMenuItem(attackSubMenu, attackMenuItem);
            }
            
            var attackSubMenuAction = instance_create(0,0,obj_SubmenuAction);
            scr_SubMenuActionSetParams(attackSubMenuAction, "Attack", attackSubMenu);
            
            var attackSubMenuItem = instance_create(0,0,obj_MenuItem);
            scr_MenuItemSetAction(attackSubMenuItem, attackSubMenuAction);
            
            scr_MenuOnAddMenuItem(menu, attackSubMenuItem);
        }
    }
}

//move
if(scr_CharacterCanMove(character))
{
    var moveAction = instance_create(0,0,obj_MoveAction);
    scr_MoveActionSetParams(moveAction, "Move", character,character.movementRange);
    
    var moveActionMenuItem = instance_create(0,0,obj_MenuItem);
    scr_MenuItemSetAction(moveActionMenuItem, moveAction);
    
    scr_MenuOnAddMenuItem(menu, moveActionMenuItem);
}

//rotate
var rotateAction = instance_create(0,0,obj_RotateAction);
scr_RotateActionSetParams(rotateAction, "Rotate", character);

var rotateActionMenuItem = instance_create(0,0,obj_MenuItem);
scr_MenuItemSetAction(rotateActionMenuItem, rotateAction);

scr_MenuOnAddMenuItem(menu, rotateActionMenuItem);

var menuContainer = instance_find(obj_MenuContainer,0);
scr_MenuContainerOnAddMenu(menuContainer, menu);
