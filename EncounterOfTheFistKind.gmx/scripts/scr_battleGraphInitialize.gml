/*ok. what is this?
A list of points with lists of other points
between each point a line is drawn
each point is drawn as an oval

Map contains list of point objects and connections
draws all
points are objects with position*/

nodes = ds_list_create();
freeNodes = ds_list_create();
connectionsMap = ds_map_create();

for(var i=0; i<instance_number(obj_GraphNode); i++)
{
    nodes[i] = instance_find(obj_GraphNode,i);
    freeNodes[| i] = nodes[i];
    connectionsMap[? nodes[i]] = ds_list_create();
}

//check how many nodes there are and add mappings

//each instance id has an array of instance ids. can use a script to feed it
//for example just connect them
var nodeCount = array_length_1d(nodes);
for(var i=0; i < nodeCount; i++)
{
    var nextNode = (i+1)%nodeCount;
    ds_list_add(connectionsMap[? nodes[i]],nodes[nextNode]);
    ds_list_add(connectionsMap[? nodes[nextNode]],nodes[i]);
}
