var paramMap = argument0;
var source = argument1;
var target = argument2;

/*
return the first position just outside the sprite based on direction

could work it out. could just lerp it til contact? inefficient.

currently messy. adds full height even if its only a slight x or y diff
*/

var xdir = 0;
var ydir = 0;
var finalX = target.x;
var finalY = target.y;

if(source.x<target.x)
{
    xdir = -1;
}
if(source.x>target.x)
{
    xdir = 1;
}
if(source.y<target.y)
{
    ydir = -1;
}
if(source.y>target.y)
{
    ydir = 1;
}

//finalX = finalX+((target.sprite_width/2)*xdir);
//finalY = finalY+((target.sprite_height/2)*ydir);

ds_map_add(paramMap,"targetX",finalX);
ds_map_add(paramMap,"targetY",finalY);
