name = "Joyce";
currentlyTakingATurn = true;
isDead = false;
attackRange = 1;
maxHealth = 300;
currentHealth = maxHealth;
strength = 30;
defense = 10;
highlighted = false;
movementRange = 1;
positionUserSetPositionCallback = scr_CharacterSetPosition;
positionUserMoveToPositionCallback = scr_TravelToPosition;
spriteDirection = SPRITE_DIRECTION_LEFT;
orientation = ORIENTATION_NORTH;
runningShader = noone;
hasPlayedDeathSound = false;
previousRunningShader = runningShader;
struckSound = snd_CharacterStruck;
runSound = snd_CharacterRun;
deadSound = snd_CharacterDie;

orientationWheel = instance_create(x,y,obj_OrientationWheel);
orientationWheel.source = id;

image_xscale = 2;
image_yscale = image_xscale;

var maxDimension = max(sprite_width,sprite_height);
scr_OrientationWheelSetDiameter(orientationWheel,maxDimension+40);

noNewSprite = -1;

idleAnimationName = "Idle";
idleAnimSpeed = 0.1;
scr_AnimationControllerAddAnimation(idleAnimationName,0,2,noNewSprite,idleAnimSpeed);

hitAnimationName = "Hit";
hitAnimSpeed = 0.5;
scr_AnimationControllerAddAnimation(hitAnimationName,2,6,noNewSprite,hitAnimSpeed);

attackAnimationName = "Attack";
attackAnimSpeed = 0.5;
scr_AnimationControllerAddAnimation(attackAnimationName,7,9,noNewSprite,attackAnimSpeed);

runAnimationName = "Run";
runAnimSpeed = 1;
scr_AnimationControllerAddAnimation(runAnimationName,10,17,noNewSprite,runAnimSpeed);
