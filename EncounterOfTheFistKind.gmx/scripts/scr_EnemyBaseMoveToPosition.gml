var enemy = argument0[? "enemy"];
var nextNode = argument0[? "nextNode"];
var newX = nextNode.x;
var newY = nextNode.y;
var returnMap = argument0[? "returnMap"];
var actionEntry = argument1;
var step = 0.02;

var originalX = ds_map_find_value(returnMap,"originalX");
if(is_undefined(originalX))
{
    originalX = enemy.x;
    ds_map_add(returnMap,"originalX",originalX);
}

var originalY = ds_map_find_value(returnMap,"originalY");
if(is_undefined(originalY))
{
    originalY = enemy.y;
    ds_map_add(returnMap,"originalY",originalY);
}

var progressDecimal = ds_map_find_value(returnMap,"progressDecimal");
if(is_undefined(progressDecimal))
{
    progressDecimal = 0;
    ds_map_add(returnMap,"progressDecimal",progressDecimal);
}
progressDecimal = progressDecimal+step;
returnMap[? "progressDecimal"] = progressDecimal;

enemy.x = lerp(originalX,newX,progressDecimal);
enemy.y = lerp(originalY,newY,progressDecimal);

var wheelX = enemy.x;
var wheelY = enemy.y - enemy.sprite_height/2;
scr_OrientationWheelSetPosition(enemy.orientationWheel,wheelX,wheelY);

if(enemy.x == newX and enemy.y == newY)
{
    scr_battleGraphMoveObjectToNode(enemy, nextNode);
    scr_ActionHandlerActionEntryComplete(actionEntry);
}
