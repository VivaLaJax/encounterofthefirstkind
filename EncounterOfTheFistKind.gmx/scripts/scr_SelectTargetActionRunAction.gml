if(runAction)
{
    var currentTarget = targetList[| currentIndex];
    if(!currentTarget.highlighted)
    {
        currentTarget.highlighted = true;
    }
}
