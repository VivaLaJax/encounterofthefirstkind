var actionHandler = argument0;

var animEntryCount = ds_list_size(actionHandler.animationEntries);
for(var i=0; i<animEntryCount; i++)
{
    with(actionHandler.animationEntries[| i])
    {
        instance_destroy();
    }
}
var actionEntryCount = ds_list_size(actionHandler.actionEntries);
for(var i=0; i<actionEntryCount; i++)
{
    with(actionHandler.actionEntries[| i])
    {
        instance_destroy();
    }
}
