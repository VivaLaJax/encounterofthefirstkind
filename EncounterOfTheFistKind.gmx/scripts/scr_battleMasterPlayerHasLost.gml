var battleMaster = instance_find(obj_battleMaster,0);

var deadCharacterCount = 0;
var characters = battleMaster.playerParty;
var characterCount = ds_list_size(characters);
for(var i=0; i<characterCount; i++)
{
    if(characters[| i].isDead)
    {
        deadCharacterCount++;
    }
}

if(characterCount==deadCharacterCount)
{
    return true;
}

return false;
