if(source == noone)
{
    return -1;
}

if(!hasPlayedSFX)
{
    scr_SoundMasterPlaySound(sfx);
    hasPlayedSFX = true;
}

//if it's behind you go left, if it's in front of you go right
if(origX>=targetX)
{
    scr_SetSpriteDirection(source,SPRITE_DIRECTION_LEFT);
}
else
{
    scr_SetSpriteDirection(source,SPRITE_DIRECTION_RIGHT);
}

timePassed = current_time-startTime;
var percentage = timePassed/timeToTravel;
if(percentage>1)
{
    percentage = 1;
}

source.x = lerp(origX, targetX, percentage);
source.y = lerp(origY, targetY, percentage);

if(source.x==targetX && source.y==targetY)
{
    show_debug_message("Travel to position is ending");
    actionEntry.actionComplete = true;
    instance_destroy();
}
