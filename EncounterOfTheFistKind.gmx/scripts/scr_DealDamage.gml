var target = argument0[? "target"];
var damage = argument0[? "damage"];
var orientation = argument0[? "orientation"];
var actionEntry = argument1;

var damageMultiplier = scr_OrientationUtilGetOrientationMultiplier(orientation, target.orientation);
damage = damage*damageMultiplier;
damage = round(damage);
show_debug_message("Target " +string(target.name)+" has taken " +string(damage)+" damage");

scr_SoundMasterPlaySound(target.struckSound);

var damageEffect = instance_create(target.x,target.y-target.sprite_height,obj_DamageEffect);
scr_DamageEffectSetValue(damageEffect,damage);

var screenShakeEffect = instance_create(target.x,target.y-target.sprite_height,obj_ScreenShakeEffect);
var characterFlashEffect = instance_create(target.x,target.y-target.sprite_height,obj_CharacterFlashEffect);
scr_CharacterFlashEffectSetParams(target,c_white,characterFlashEffect);

target.currentHealth = target.currentHealth-damage;
if(target.currentHealth<=0)
{
    scr_ActionHandlerInsertAnimationEntry(actionEntry.actionHandler,target,"Death",ANIMATION_PLAY_ONCE);
    scr_battleMasterOnCharacterDied(target);
}
else
{
    scr_ActionHandlerInsertAnimationEntry(actionEntry.actionHandler,target,"Hit",ANIMATION_PLAY_ONCE);
}

scr_ActionHandlerActionEntryComplete(actionEntry);
