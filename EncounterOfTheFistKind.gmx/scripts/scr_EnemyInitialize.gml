name = "Beach Bol"
isDead = false;
maxHealth = 200;
currentHealth = maxHealth;
strength = 20;
defense = 10;
attackRange = 1;
highlighted = false;
movementRange = 1;
turnStartCallback = scr_EnemyOnStartTurn;
isTakingTurn = false;
spriteDirection = SPRITE_DIRECTION_LEFT;
runningShader = noone;
previousRunningShader = runningShader;
hasPlayedDeathSound = false;
struckSound = snd_EnemyStruck;
runSound = snd_EnemyRun;
deadSound = snd_EnemyDie;

positionUserSetPositionCallback = scr_EnemyBaseSetPosition;
positionUserMoveToPositionCallback = scr_TravelToPosition;

orientation = ORIENTATION_NORTH;
orientationWheel = instance_create(x,y,obj_OrientationWheel);
orientationWheel.source = id;
var maxDimension = max(sprite_width,sprite_height);
scr_OrientationWheelSetDiameter(orientationWheel,maxDimension+40);

noNewSprite = -1;

deathAnimationName = "Death";
deathAnimStart = false;
deathAnimSpeed = 1;
scr_AnimationControllerAddAnimation(deathAnimationName,0,15,noNewSprite,deathAnimSpeed);

idleAnimationName = "Idle";
idleAnimStart = false;
idleAnimSpeed = 0.05;
scr_AnimationControllerAddAnimation(idleAnimationName,16,18,noNewSprite,idleAnimSpeed);

runAnimationName = "Run";
runAnimStart = false;
runAnimSpeed = 0.3;
scr_AnimationControllerAddAnimation(runAnimationName,16,18,noNewSprite,runAnimSpeed);

attackAnimationName = "Attack";
attackAnimStart = false;
attackAnimSpeed = 0.5;
scr_AnimationControllerAddAnimation(attackAnimationName,18,22,noNewSprite,attackAnimSpeed);
