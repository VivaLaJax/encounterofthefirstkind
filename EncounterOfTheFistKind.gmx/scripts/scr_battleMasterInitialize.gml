/* covers the whole of the battle

A battle is a party of characters taking turns until one group is all dead

places characters
sets up the turn order
inform character when turn starts
is informed when a character dies
traverses to appropriate screen when win condition is fulfilled

*/
currentUIFocus = ds_list_create();
roundNumber = 0;

turnOrder = ds_list_create();
enemyCharacters = ds_list_create();
playerParty = ds_list_create();

hasPlayedSpedMusic = false;

//get party
var player = instance_find(obj_Player, 0);
playerParty = player.party;

//create enemies
var enemyNumber = 2;
for(var i=0; i< enemyNumber; i++)
{
    enemyCharacters[i] = instance_create(0,0,obj_Enemy);
}

//create turn order for first two rounds
//should calculate number of rounds rather than grabbing the next round
turnOrder = scr_battleMasterCalculateNextRound(playerParty,enemyCharacters);

//place characters
for(var i=0; i<enemyNumber; i++)
{
    var nextNode = scr_battleGraphGetNextOpenNode();
    scr_battleGraphNodeFill(nextNode,enemyCharacters[i]);
    scr_EnemyBaseSetPosition(enemyCharacters[i],nextNode.x, nextNode.y);
}

var partyNumber = ds_list_size(playerParty);
for(var i=0; i<partyNumber; i++)
{
    var nextNode = scr_battleGraphGetNextOpenNode();
    scr_battleGraphNodeFill(nextNode,playerParty[| i]);
    scr_CharacterSetPosition(-1,playerParty[| i],nextNode.x, nextNode.y);
}

scr_battleMasterStartNextTurn(id);
