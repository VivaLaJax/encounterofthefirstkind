var source = argument0;
var animationName = argument1;
var playType = argument2;
var animationRequestIndex = 0;

source.playMode = playType;

if(source.playMode==ANIMATION_STOPPED)
{
    return 0;
}

if(ds_list_find_index(source.animations,animationName)!=-1)
{
    source.currentAnimation = ds_list_find_index(source.animations,animationName);
    source.currentSpriteIndex = source.animationStartMap[? animationName];
    if(ds_map_exists(source.animationSpriteMap,animationName))
    {
        sprite_index = source.animationSpriteMap[? animationName];
    }
    source.animationComplete = false;
    animationRequestIndex = source.animationRequestIndex;
    source.animationRequestIndex = source.animationRequestIndex+1;
}

return animationRequestIndex;
