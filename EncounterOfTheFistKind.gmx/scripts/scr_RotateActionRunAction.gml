if(selectTargetAction.runAction!=runAction)
{
    selectTargetAction.runAction = runAction;
    if(selectTargetAction.runAction)
    {
        scr_OrientationWheelShowAllNodes(source.orientationWheel, true);
        scr_battleMasterPushUIFocus(selectTargetAction);
    }
    else
    {
        scr_OrientationWheelHideAllButSelected(source.orientationWheel);
    }
}

if(runAction)
{    
    if(selectedTarget!=0)
    {
        source.orientation = selectedTarget.orientation;
        scr_OrientationWheelHideAllButSelected(source.orientationWheel);

        runAction = false;
        selectTargetAction.runAction = false;
        scr_battleMasterOnActionCompleted(self);
    }
}
