var battleMaster = argument0;
var player = instance_find(obj_Player,0);

var currentTurnTaker = battleMaster.turnOrder[| 0];
if(ds_list_find_index(player.party,currentTurnTaker)!=-1)
{
    var menuContainer = instance_find(obj_MenuContainer,0);
    menuContainer.isLocked = false;
    ds_list_add(battleMaster.currentUIFocus,menuContainer);
    scr_CharacterOnTurnStart(battleMaster.turnOrder[| 0]);
}
else
{
    //should pass the start to an enemy for it to run in step
    script_execute(currentTurnTaker.turnStartCallback,battleMaster.turnOrder[| 0]);
}
