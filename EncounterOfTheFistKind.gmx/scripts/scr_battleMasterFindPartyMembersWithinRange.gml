var battleMaster = instance_find(obj_battleMaster,0);
var nodeA = argument0;
var range = argument1;

var partyNumber = ds_list_size(battleMaster.playerParty);
var party = ds_list_create();
for(var i=0; i< partyNumber; i++)
{
    if(battleMaster.playerParty[| i].isDead)
    {
        continue;
    }
    var nodeB = scr_battleGraphGetGraphNodeByObject(battleMaster.playerParty[| i]);
    if(nodeB==-1 )
    {
        continue;
    }
    var distance = scr_battleGraphNodeDistance(nodeA, nodeB,ds_list_create());
    if(distance!=-1 && distance<=range)
    {
        ds_list_add(party, battleMaster.playerParty[| i]);
    }
}

return party;
