directionNodes = ds_list_create();
diameter = 0;
source = 0;
currentNode = 0;

numDirections = 8;
for(var i=0; i<numDirections; i++)
{
    var directionNode = instance_create(x,y,obj_DirectionNode);
    if(i==currentNode)
    {
        directionNode.hidden=false;
    }
    directionNode.orientation = i;
    ds_list_add(directionNodes, directionNode);
}
