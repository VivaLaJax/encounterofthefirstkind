event_inherited();

selectTargetAction = instance_create(0,0,obj_SelectTargetAction);
selectedTarget = 0;
source = 0;

//every char has an orientation wheel and direction
//orientation wheel has direction nodes
//direction nodes have highlightable sprite/draw call and direction
//rotate action has select target action with wheel nodes as targets
//when running it sets target action as focus and tells wheel to show all
//when selected wheel has selected node set to target and character has node direction

//gotta work out setting up starting orientation
//and having the select start from the current char orientation
