var actionHandler = argument0;
var animationSource = argument1;
var animationName = argument2;
var animPlayType = argument3;

var entry = instance_create(x,y,obj_ActionHandlerAnimationEntry);
scr_ActionHandlerAnimationEntrySetValues(entry,animationSource,animationName,animPlayType,actionHandler);
ds_list_add(actionHandler.animationEntries,entry);
if(actionHandler.entryIndex==INITIAL_VALUE_NUM)
{
    actionHandler.entryIndex = 0;
}
ds_list_add(actionHandler.entryOrder,entry.entryIndex);

ds_list_add(actionHandler.completedEntries, entry);
show_debug_message("Anim++ Completed count: "+string(ds_list_size(actionHandler.completedEntries)));
show_debug_message("Animation entry added: "+animationName);

actionHandler.entryIndex++;
actionHandler.entryCount++;
