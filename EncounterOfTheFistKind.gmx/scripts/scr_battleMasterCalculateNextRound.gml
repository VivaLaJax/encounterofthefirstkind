var playerParty = argument0;
var enemies = argument1;
var turnOrder = ds_list_create();

//keep it simple and interleave for now
var partyNumber = ds_list_size(playerParty);
var enemyNumber = array_length_1d(enemies);
var totalCharacters = partyNumber + enemyNumber;

show_debug_message("Round Calc: All Characters - "+string(totalCharacters));

var partyIndex = 0;
var enemyIndex = 0;
var team = 0;
var turnOrderIndex = 0;
for(var i=0; i<totalCharacters; i++)
{
    show_debug_message("Round Calc: Team = "+string(team) + " CharIndex = "+ string(i) + " PartyIndex = "+ string(partyIndex)+"/"+string(partyNumber)+" EnemyIndex= "+string(enemyIndex)+"/"+string(enemyNumber));
    if(team==0 and partyIndex<partyNumber)
    {
        if(!playerParty[| partyIndex].isDead)
        {
            show_debug_message("Round Calc: Adding party member - "+string(i));
            turnOrder[| turnOrderIndex] = playerParty[| partyIndex];
            turnOrderIndex++;
            team = (team+1)%2;
        }
        partyIndex++;
        continue;
    }
    if(enemyIndex<enemyNumber)
    {
        if(!enemies[enemyIndex].isDead)
        {
            show_debug_message("Round Calc: Adding enemy - "+string(i));
            turnOrder[| turnOrderIndex] = enemies[enemyIndex];
            turnOrderIndex++;
        }
        enemyIndex++;
    }
    
    team = (team+1)%2;
}

return turnOrder;
