var homeNode = scr_battleGraphGetGraphNodeByObject(id);
var pathToClosestParty = scr_battleMasterGetPathToClosestPartyMember(homeNode);
scr_DebugPrintList(pathToClosestParty,"Path");
var nextIndex = 1;

var handler = instance_create(x,y,obj_ActionHandler);
handler.actionSource = id;
var paramMap = ds_map_create();

scr_ActionHandlerAddAnimationEntry(handler,id, "Run", ANIMATION_REPEAT);

var travelParametersMap = ds_map_create();
ds_map_add(travelParametersMap,"source",id);
var nextNode = pathToClosestParty[| nextIndex];
ds_map_add(travelParametersMap,"targetX",nextNode.x);
ds_map_add(travelParametersMap,"targetY",nextNode.y);
ds_map_add(travelParametersMap,"time",1000);
ds_map_add(travelParametersMap,"sfx",runSound);
scr_ActionHandlerAddActionEntry(handler,positionUserMoveToPositionCallback,travelParametersMap,"MoveAction");

scr_ActionHandlerAddAnimationEntry(handler,id, "Idle", ANIMATION_REPEAT);

var wheelX = nextNode.x;
var wheelY = nextNode.y - sprite_height/2;
scr_OrientationWheelSetPosition(orientationWheel,wheelX,wheelY);
scr_battleGraphMoveObjectToNode(id, nextNode);
scr_ActionHandlerStart(handler);
