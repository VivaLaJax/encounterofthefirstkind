var menuContainer = instance_find(obj_MenuContainer,0);

var menuCount = ds_list_size(menuContainer.menuStack);
for(var i=0; i<menuCount; i++)
{
    scr_MenuCleanMenu(menuContainer.menuStack[| i]);
}

ds_list_clear(menuContainer.menuStack);
scr_MenuContainerResetPosition(menuContainer);
