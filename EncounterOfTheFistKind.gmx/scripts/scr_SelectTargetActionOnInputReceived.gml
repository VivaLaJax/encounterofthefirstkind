var selectTargetAction = argument0;
var input = argument1;

if(input==INPUT_UP ||
 input==INPUT_LEFT ||
 input==INPUT_NW ||
 input==INPUT_NE)
{
    selectTargetAction.targetList[| selectTargetAction.currentIndex].highlighted=false;
    if(selectTargetAction.currentIndex==0)
    {
        selectTargetAction.currentIndex = ds_list_size(selectTargetAction.targetList)-1;
    }
    else
    {
        selectTargetAction.currentIndex = selectTargetAction.currentIndex - 1;
    }
    return 0;
}

if(input==INPUT_DOWN ||
 input==INPUT_RIGHT ||
 input==INPUT_SW ||
 input==INPUT_SE)
{
    selectTargetAction.targetList[| selectTargetAction.currentIndex].highlighted=false;
    var targetCount = ds_list_size(selectTargetAction.targetList);
    selectTargetAction.currentIndex = (selectTargetAction.currentIndex+1)%targetCount;
    return 0;
}

if(input==INPUT_ENTER)
{
    selectTargetAction.parentAction.selectedTarget = selectTargetAction.targetList[| selectTargetAction.currentIndex];
    scr_SelectTargetActionClearHighlights(selectTargetAction);
    return 0;
}

if(input==INPUT_CANCEL)
{
    selectTargetAction.parentAction.runAction = false;
    scr_SelectTargetActionClearHighlights(selectTargetAction);
    scr_battleMasterPopUIFocus();
    var menuContainer = instance_find(obj_MenuContainer,0);
    scr_MenuContainerOnActionCancelled(menuContainer);
    return 0;
}
