var battleMaster = argument0;
var input = argument1;

var focusStackSize = ds_list_size(battleMaster.currentUIFocus);
if(focusStackSize>0)
{
    var currentFocus = battleMaster.currentUIFocus[| focusStackSize-1];
    script_execute(currentFocus.inputUserCallback,currentFocus, input);
}

