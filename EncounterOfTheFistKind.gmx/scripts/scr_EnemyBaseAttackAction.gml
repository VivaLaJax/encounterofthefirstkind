var homeNode = scr_battleGraphGetGraphNodeByObject(id);
var enemiesWithinRange = scr_battleMasterFindPartyMembersWithinRange(homeNode, attackRange);
var enemiesCount = ds_list_size(enemiesWithinRange);
if(enemiesCount!=0)
{
    var target = 0;
    if(enemiesCount>1)
    {
        target = random_range(0,enemiesCount);
        target = round(target);
        if(target==enemiesCount)
        {
            target = enemiesCount-1;
        }
    }
    
    var damage = 5*(strength-enemiesWithinRange[| target].defense);
    var handler = instance_create(x,y,obj_ActionHandler);
    handler.actionSource = id;
    
    scr_ActionHandlerAddAnimationEntry(handler,id, "Attack", ANIMATION_PLAY_ONCE);
    
    var attackParamMap = ds_map_create();
    ds_map_add(attackParamMap,"target",enemiesWithinRange[| target]);
    ds_map_add(attackParamMap,"source",id);
    ds_map_add(attackParamMap,"projectile",obj_EnemySpit);
    ds_map_add(attackParamMap,"timeToReach",1000);
    scr_ActionHandlerAddActionEntry(handler,scr_ThrowProjectile,attackParamMap,"ThrowProjectile");
    
    var paramMap = ds_map_create();
    ds_map_add(paramMap,"target",enemiesWithinRange[| target]);
    ds_map_add(paramMap,"damage",damage);
    ds_map_add(paramMap,"orientation",orientation);
    scr_ActionHandlerAddActionEntry(handler,scr_DealDamage,paramMap,"DealDamage");
    
     scr_ActionHandlerAddAnimationEntry(handler,id, "Idle", ANIMATION_REPEAT);
     
    scr_ActionHandlerStart(handler);
}
