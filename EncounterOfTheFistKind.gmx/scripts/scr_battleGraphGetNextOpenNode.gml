var battleMap = instance_find(obj_battleGraph, 0);

var numberFreeNodes = ds_list_size(battleMap.freeNodes);

//could be random
if(numberFreeNodes>0)
{
    return battleMap.freeNodes[| 0];
}

return -1;
