var animationEntry = argument0;
var animationSource = argument1;
var animationName = argument2;
var animPlayType = argument3;
var actionHandler = argument4;

animationEntry.animationSource = animationSource;
animationEntry.animationName = animationName;
animationEntry.animPlayType = animPlayType;
animationEntry.entryIndex = actionHandler.entryIndex;
animationEntry.actionHandler = actionHandler;
