var actionHandler = argument0;

var entry = noone;
var index = actionHandler.entryOrder[| actionHandler.currentEntryIndex];
var count = ds_list_size(actionHandler.actionEntries);
for(var i=0; i<count; i++)
{
    if(actionHandler.actionEntries[| i].entryIndex==index)
    {
        entry = actionHandler.actionEntries[| i];
        break;
    }
}

entry.runAction = true;
