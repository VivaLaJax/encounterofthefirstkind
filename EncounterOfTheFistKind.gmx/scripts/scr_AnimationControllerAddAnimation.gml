var animationName = argument0;
var animStartIndex = argument1;
var animEndIndex = argument2;
var animSprite = argument3;
var animSpeed = argument4;

if(ds_list_find_index(animations,animationName)==-1)
{
    ds_list_add(animations,animationName);
    ds_map_add(animationStartMap, animationName, animStartIndex);
    ds_map_add(animationEndMap, animationName, animEndIndex);
    ds_map_add(animationSpeedMap, animationName, animSpeed);
    if(animSprite!=-1)
    {
        ds_map_add(animationSpriteMap, animationName, animSprite);
    }
}
