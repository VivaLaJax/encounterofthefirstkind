var animationSource = argument0;
var animationRequestIndex = argument1;

show_debug_message("Animation Index= "+string(animationRequestIndex));
show_debug_message("Source animation index= "+string(animationSource.animationRequestIndex));
show_debug_message("Animation Complete= "+string(animationSource.animationComplete));

return animationSource.animationRequestIndex>=animationRequestIndex
|| (animationRequestIndex==animationSource.animationRequestIndex && 
animationSource.animationComplete);
