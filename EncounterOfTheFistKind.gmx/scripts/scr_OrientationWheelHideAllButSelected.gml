var orientationWheel = argument0;

for(var i=0; i<orientationWheel.numDirections; i++)
{
    if(i!=orientationWheel.currentNode)
    {
        orientationWheel.directionNodes[| i].hidden = true;
    }
}
