var source = argument0;
var shader = argument1;

if(shader_is_compiled(shader))
{
    source.runningShader = shader;
}
