//A character can attack if there is an enemy within their range
var character = argument0;

var homeNode = scr_battleGraphGetGraphNodeByObject(character);
if(homeNode==-1)
{
    return false;
}
var enemiesWithinRange = scr_battleMasterFindEnemiesWithinRange(homeNode, character.attackRange);
if(ds_list_size(enemiesWithinRange)>0)
{
    return true;
}

return false;
