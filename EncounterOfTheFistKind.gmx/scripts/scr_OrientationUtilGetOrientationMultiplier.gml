var orientationA = argument0;
var orientationB = argument1;

if(orientationA==orientationB)
{
    return 1.5;
}

if((orientationA==ORIENTATION_NORTH && orientationB==ORIENTATION_SOUTH) ||
(orientationA==ORIENTATION_SOUTH && orientationB==ORIENTATION_NORTH))
{
    return 0.75;
}

if((orientationA==ORIENTATION_EAST && orientationB==ORIENTATION_WEST) ||
(orientationA==ORIENTATION_WEST && orientationB==ORIENTATION_EAST))
{
    return 0.75;
}

return 1;
