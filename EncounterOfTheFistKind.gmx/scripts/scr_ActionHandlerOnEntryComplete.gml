var actionHandler = argument0;
var entry = argument1;

show_debug_message("Attempting completion of "+string(id));
scr_DebugPrintList(actionHandler.completedEntries,"Completed Entries");
var entryIndex = ds_list_find_index(actionHandler.completedEntries,entry);
ds_list_delete(actionHandler.completedEntries,entryIndex);
show_debug_message("Entry index: "+string(entryIndex)+" Completed count: "+string(ds_list_size(actionHandler.completedEntries)));
actionHandler.currentEntryIndex++;
actionHandler.readyForNextEntry = true;
