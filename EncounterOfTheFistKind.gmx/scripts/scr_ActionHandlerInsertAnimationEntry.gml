var actionHandler = argument0;
var animationSource = argument1;
var animationName = argument2;
var animPlayType = argument3;

var entry = instance_create(x,y,obj_ActionHandlerAnimationEntry);
scr_ActionHandlerAnimationEntrySetValues(entry,animationSource,animationName,animPlayType,actionHandler);
ds_list_add(actionHandler.animationEntries,entry);
ds_list_insert(actionHandler.entryOrder,actionHandler.currentEntryIndex+1,entry.entryIndex);

ds_list_add(actionHandler.completedEntries, entry);
show_debug_message("Anim++ Completed count: "+string(ds_list_size(actionHandler.completedEntries)));
show_debug_message("Animation entry added: "+animationName);

actionHandler.entryIndex++;
actionHandler.entryCount++;
