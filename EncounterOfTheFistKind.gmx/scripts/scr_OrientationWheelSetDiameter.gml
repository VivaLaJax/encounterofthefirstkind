var orientationWheel = argument0;
var diameter = argument1;

orientationWheel.diameter = diameter;

var slice = (2*pi)/orientationWheel.numDirections;
var quarterSlice = pi/2;
for(var i=0; i<orientationWheel.numDirections; i++)
{
    var angle = (slice*i)-quarterSlice;
    //these should be functions of the circle and diam
    var circX = orientationWheel.x+(diameter/2)*cos(angle);
    var circY = orientationWheel.y+(diameter/2)*sin(angle);
    orientationWheel.directionNodes[| i].x = circX;
    orientationWheel.directionNodes[| i].y = circY;
}
