var actionHandler = argument0;

var index = actionHandler.entryOrder[| actionHandler.currentEntryIndex];
var count = ds_list_size(actionHandler.animationEntries);
for(var i=0; i<count; i++)
{
    if(actionHandler.animationEntries[| i].entryIndex==index)
    {
        show_debug_message("selected anim index = "+string(actionHandler.animationEntries[| i].entryIndex)+". Entry index= "+string(index));
        return true;
    }
}

return false;
