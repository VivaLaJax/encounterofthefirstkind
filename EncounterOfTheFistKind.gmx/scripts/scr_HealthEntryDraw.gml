var lineColour = c_ltgray;
draw_line_colour(x+1,
                 y+entryHeight,
                 x+entryWidth-1,
                 y+entryHeight,
                 lineColour,
                 lineColour);

var nameHeight = string_height(sourceName);
var nameWidth = string_width(sourceName);
var buffer = 3;
var nameX = x+buffer;
draw_text(nameX, y+(entryHeight/2)-nameHeight/2,sourceName);

var numberString = string(currentHealth);
numberString = string_insert("/",numberString,string_length(numberString)+1);
numberString = string_insert(string(maxHealth),numberString,string_length(numberString)+1);

var numStringHeight = string_height(numberString);
var numStringWidth = string_width(numberString);
var numX = nameWidth + nameX + 3*buffer;
draw_text(numX,y+(entryHeight/2)-numStringHeight/2,numberString);

var boxHeight = numStringHeight;
var boxX = numX+numStringWidth+2*buffer;
var boxY = y+(entryHeight/2)-(boxHeight/2);
var boxWidth = entryWidth - boxX-2*buffer;
var percent = currentHealth/maxHealth;
var colour = c_green;
if(percent<0.6)
{
    colour = c_orange;
}
else if(percent<0.3)
{
    colour = c_red;
}

draw_rectangle_colour(boxX,
                      boxY,
                      boxX+boxWidth,
                      boxY+boxHeight,
                      c_dkgray,
                      c_dkgray,
                      c_dkgray,
                      c_dkgray,
                      false);
draw_rectangle_colour(boxX,
                      boxY,
                      boxX + boxWidth*percent,
                      boxY+boxHeight,
                      colour,
                      colour,
                      colour,
                      colour,
                      false);
