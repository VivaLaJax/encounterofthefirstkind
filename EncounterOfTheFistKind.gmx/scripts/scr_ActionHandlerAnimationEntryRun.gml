if(runAnimation)
{
    if(!animationStarted)
    {
        show_debug_message("Animation beginning: "+animationName);
        animationRequestIndex = scr_AnimationControllerPlayAnimation(animationSource,animationName,animPlayType);
        animationStarted = true;
        if(animPlayType==ANIMATION_REPEAT)
        {
            scr_ActionHandlerOnEntryComplete(actionHandler, id);
            runAnimation = false;
        }
        return 0;
    }
    if(scr_AnimationControllerCheckComplete(animationSource,animationRequestIndex))
    {
        show_debug_message("Animation complete: "+animationName);
        scr_ActionHandlerOnEntryComplete(actionHandler, id);
        runAnimation = false;
        animationSource.animationComplete = false;
    }
}
