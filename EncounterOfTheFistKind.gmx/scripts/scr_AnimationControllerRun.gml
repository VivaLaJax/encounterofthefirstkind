if(playMode==ANIMATION_STOPPED)
{
    return 0;
}

if(!is_undefined(ds_list_find_value(animations,currentAnimation)))
{
    var animName = animations[| currentAnimation];
    image_index = currentSpriteIndex;
    currentSpriteIndex=currentSpriteIndex+animationSpeedMap[? animName];
    //should tie this lot to time
    if(currentSpriteIndex>=animationEndMap[? animName])
    {
        if(playMode==ANIMATION_PLAY_ONCE)
        {
            animationComplete = true;
            //could have an animation queue here
            playMode=ANIMATION_STOPPED;
        }
        else if(playMode==ANIMATION_REPEAT)
        {
            currentSpriteIndex=ds_map_find_value(animationStartMap,animName);
        }
    }
}
