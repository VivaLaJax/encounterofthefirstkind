if(targetSelectionAction.runAction!=runAction)
{
    targetSelectionAction.runAction = runAction;
    if(targetSelectionAction.runAction)
    {
        scr_battleMasterPushUIFocus(targetSelectionAction);
    }
}

if(runAction)
{    
    if(selectedTarget!=0)
    {
        var actionHandler = instance_create(x,y,obj_ActionHandler);
        actionHandler.actionSource = id;
              
        scr_battleGraphMoveObjectToNode(source, selectedTarget);
        var travelParametersMap = ds_map_create();
        ds_map_add(travelParametersMap,"source",source);
        ds_map_add(travelParametersMap,"targetX",selectedTarget.x);
        ds_map_add(travelParametersMap,"targetY",selectedTarget.y);
        ds_map_add(travelParametersMap,"time",1000);
        ds_map_add(travelParametersMap,"sfx",source.runSound);
        scr_ActionHandlerAddAnimationEntry(actionHandler,source, "Run", ANIMATION_REPEAT);
        scr_ActionHandlerAddActionEntry(actionHandler,source.positionUserMoveToPositionCallback,travelParametersMap,"TravelToPos");
        
        var wheelX = selectedTarget.x;
        var wheelY = selectedTarget.y - source.sprite_height/2;
        scr_OrientationWheelSetPosition(source.orientationWheel,wheelX,wheelY);

        scr_ActionHandlerAddAnimationEntry(actionHandler,source, "Idle", ANIMATION_REPEAT);
    
        scr_ActionHandlerStart(actionHandler);
        
        runAction = false;
        targetSelectionAction.runAction = false;
    }
}
