var menuItem = argument0;
menuItem.action.runAction = true;

scr_SoundMasterPlaySound(snd_SelectOption);

if(!menuItem.action.isMenuAction)
{
    var menuContainer = instance_find(obj_MenuContainer,0);
    scr_MenuContainerOnActionFinalized(menuContainer);
}
