var enemy = argument0;
var newX = argument1;
var newY = argument2;

enemy.x = newX;
enemy.y = newY;
var wheelX = newX;
var wheelY = newY - enemy.sprite_height/2;
scr_OrientationWheelSetPosition(enemy.orientationWheel,wheelX,wheelY);
