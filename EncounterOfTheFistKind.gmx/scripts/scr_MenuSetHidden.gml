var menu = argument0;
var hide = argument1;

menu.hide = hide;
var menuItenCount = ds_list_size(menu.menuItems);

for(var i=0; i<menuItenCount; i++)
{
    menu.menuItems[| i].hide = hide;
}
