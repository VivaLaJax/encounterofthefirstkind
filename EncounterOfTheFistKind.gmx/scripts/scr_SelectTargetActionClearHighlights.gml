var selectTargetAction = argument0;

var targetCount = ds_list_size(selectTargetAction.targetList);
for(var i=0; i<targetCount; i++)
{
    var currentTarget = selectTargetAction.targetList[| i];
    if(currentTarget.highlighted)
    {
        currentTarget.highlighted = false;
    }
}
