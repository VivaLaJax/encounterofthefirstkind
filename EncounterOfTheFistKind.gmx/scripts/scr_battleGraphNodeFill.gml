var battleGraph = instance_find(obj_battleGraph,0);
var node = argument0;
var objectToFill = argument1;

node.objectContained = objectToFill;

var freeNodesCount = ds_list_size(battleGraph.freeNodes);
for(var i=0; i<freeNodesCount; i++)
{
    if(battleGraph.freeNodes[| i]==node)
    {
        ds_list_delete(battleGraph.freeNodes,i);
        return 0;
    }
}
