if(isDead)
{
    if(!hasPlayedDeathSound)
    {
        scr_SoundMasterPlaySound(deadSound);
        hasPlayedDeathSound = true;
    }
    
    if(animationComplete)
    {
        visible = false;
        scr_OrientationWheelShowAllNodes(orientationWheel,false);
    } 
}
