if(source.orientation!=currentNode)
{
    directionNodes[| currentNode].hidden = true;
    currentNode = source.orientation;
    directionNodes[| currentNode].hidden = false;
}
