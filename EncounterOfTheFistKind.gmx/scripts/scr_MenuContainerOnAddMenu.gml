var menuContainer = argument0;
var menu = argument1;

ds_list_add(menuContainer.menuStack, menu);

var menuStackCount = ds_list_size(menuContainer.menuStack);
menuContainer.currentMenuIndex = menuStackCount-1;

//set the xposition of the menu
//what side?
var x_pos = menuContainer.x;
if(menuContainer.currentMenuIndex!=0)
{
    x_pos = menuContainer.menuStack[| menuContainer.currentMenuIndex-1].x;
}
else
{
    menuContainer.y = menuContainer.y - menuContainer.menuStack[| menuContainer.currentMenuIndex].menuHeight;
}

var menuX = x_pos - menuContainer.menuStack[| menuContainer.currentMenuIndex].menuWidth;
//either move it up or down or neither
var menuY = menuContainer.y;
scr_MenuSetPosition(menuContainer.menuStack[| menuContainer.currentMenuIndex],menuX,menuY);
scr_MenuSetHidden(menuContainer.menuStack[| menuContainer.currentMenuIndex],false);
