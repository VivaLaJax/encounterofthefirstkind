var turnboxWidth = display_get_gui_width()/10;
var turnboxHeight = display_get_gui_height()/20;
var turnboxX = display_get_gui_width() - 2*turnboxWidth;
var buffer = 3;
var textYBase = (turnboxHeight/4)-buffer;
var titleOffset = turnboxHeight/2;
var turn = "Turn";

//draw the turn order
draw_text(turnboxX+buffer, buffer, turn);
    
var turnOrderNumber = ds_list_size(turnOrder);
for(var i=0; i<turnOrderNumber; i++)
{
    var turnBoxY = titleOffset+(i*turnboxHeight)+buffer;
    if(i==0)
    {
        draw_rectangle_colour(turnboxX+1,
                              turnBoxY+1,
                              turnboxX+turnboxWidth,
                              turnBoxY+turnboxHeight,
                              c_orange,
                              c_orange,
                              c_orange,
                              c_orange,
                              false);
    }
    else
    {
        draw_rectangle_colour(turnboxX+1,
                              turnBoxY+1,
                              turnboxX+turnboxWidth,
                              turnBoxY+turnboxHeight,
                              c_gray,
                              c_gray,
                              c_gray,
                              c_gray,
                              false);
    }
    
    draw_rectangle(turnboxX,turnBoxY,turnboxX+turnboxWidth,turnBoxY+turnboxHeight,true);
    draw_text(turnboxX+buffer, textYBase+turnBoxY, turnOrder[| i].name);
}
