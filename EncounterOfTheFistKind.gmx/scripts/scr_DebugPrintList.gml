var list = argument0;
var name = argument1;
var count = ds_list_size(list);

for(var i=0; i<count i++)
{
    show_debug_message(string(name)+" List @INDEX: " + string(i)+ " is " + string(list[| i]));
}
