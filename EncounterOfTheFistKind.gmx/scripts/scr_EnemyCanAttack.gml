var enemy = argument0;

var homeNode = scr_battleGraphGetGraphNodeByObject(enemy);
if(homeNode==-1)
{
    return false;
}
var enemiesWithinRange = scr_battleMasterFindPartyMembersWithinRange(homeNode, enemy.attackRange);
if(ds_list_size(enemiesWithinRange)>0)
{
    return true;
}

return false;
