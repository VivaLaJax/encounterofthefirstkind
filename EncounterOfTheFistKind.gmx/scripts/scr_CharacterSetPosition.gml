var action = argument0;
var character = argument1;
var newX = argument2;
var newY = argument3;

character.x = newX;
character.y = newY;
var wheelX = newX;
var wheelY = newY - character.sprite_height/2;
scr_OrientationWheelSetPosition(character.orientationWheel,wheelX,wheelY);

if(action!=-1)
{
    scr_battleMasterOnActionCompleted(action);
}
