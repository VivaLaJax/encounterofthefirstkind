if(runAction)
{
    if(!actionStarted)
    {
        show_debug_message("Action beginning: "+actionName);
        script_execute(actionScript,parameterMap,id);
        actionStarted = true;
    }

    if(actionComplete)
    {
        show_debug_message("Action complete: "+actionName + " id "+string(id));
        scr_ActionHandlerOnEntryComplete(actionHandler, id);
        runAction = false;
    }
}
