var battleGraph = instance_find(obj_battleGraph,0);
var nodeA = argument0;
var nodeB = argument1;
var nodesVisited = argument2;

var lowestDistanceFound = -1;
var currentDistanceFound = -1;

if(ds_list_find_index(nodesVisited,nodeA)!=-1)
{
    return -1;
}
ds_list_add(nodesVisited,nodeA);

var connectedNodesFromA = battleGraph.connectionsMap[? nodeA];
var nodesFromACount = ds_list_size(connectedNodesFromA);

if(nodesFromACount==0)
{
    return -1;
}

for(var i=0; i<nodesFromACount; i++)
{
    if(connectedNodesFromA[| i]==nodeB)
    {
        return 1;
    }
}

for(var i=0; i<nodesFromACount; i++)
{
    var lengthOfRouteFromNextNode = scr_battleGraphNodeDistance(connectedNodesFromA[| i],nodeB,nodesVisited);
    if(lengthOfRouteFromNextNode!=-1)
    {
        currentDistanceFound = lengthOfRouteFromNextNode + 1;
        if(currentDistanceFound<lowestDistanceFound || lowestDistanceFound==-1)
        {
            lowestDistanceFound = currentDistanceFound;
        }
    }
}

return lowestDistanceFound;
