if(playMode==ANIMATION_STOPPED and !isDead)
{
    scr_AnimationControllerPlayAnimation(id,"Idle",ANIMATION_REPEAT);
}

if(isTakingTurn)
{
     var homeNode = scr_battleGraphGetGraphNodeByObject(id);
     if(homeNode==-1)
     {
        return 0;
     }
    //find out if any party members are within attack range
    //if yes, should attack, otherwise should move
    //get path to closest party member and move to first need
    if(scr_EnemyCanAttack(id))
    {
        scr_EnemyBaseAttackAction();
        isTakingTurn = false;
        return 0;
    }
    
    var moving = true;
    if(moving)
    {
        scr_EnemyBaseMoveAction();
        isTakingTurn = false;
        return 0;
    }
    //should probably also rotate
    scr_battleMasterOnActionCompleted(id);
    isTakingTurn = false;
}

if(previousRunningShader!=runningShader)
{
    if(runningShader==noone)
    {
        shader_reset();
    }
    else
    {
        shader_set(runningShader);
    }
    
    previousRunningShader = runningShader;
}
