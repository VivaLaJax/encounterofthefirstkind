var menuContainer = argument0;
var input = argument1;

if(menuContainer.isLocked)
{
    return 0;
}
var actioned = scr_MenuOnInputReceived(menuContainer.menuStack[| menuContainer.currentMenuIndex], input);
//if it's not been actioned then we know we want to handle cancels
if(!actioned and input==INPUT_CANCEL)
{
    scr_MenuContainerOnMenuRemoved(menuContainer);
}
