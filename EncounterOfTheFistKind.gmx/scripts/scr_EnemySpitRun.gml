if(fired)
{
    timePassed = current_time-startTime;
    var percentage = timePassed/timeToTravel;
    if(percentage>1)
    {
        percentage = 1;
    }
    
    x = lerp(origX, targetX, percentage);
    y = lerp(origY, targetY, percentage);
    
    if(x==targetX && y==targetY)
    {
        show_debug_message("Enemy spit is ending");
        actionEntry.actionComplete = true;
        instance_destroy();
    }
}
