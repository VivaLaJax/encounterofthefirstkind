var rotateAction = argument0;
var name = argument1;
var source = argument2;

rotateAction.name = name;
rotateAction.source = source;

scr_SelectTargetActionSetParams(rotateAction.selectTargetAction,
                                source.orientationWheel.directionNodes,
                                rotateAction);
