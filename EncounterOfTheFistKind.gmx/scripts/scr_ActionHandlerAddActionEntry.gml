var actionHandler = argument0;
var actionScript = argument1;
var parameterMap = argument2;
var actionName = argument3;

var entry = instance_create(x,y,obj_ActionHandlerActionEntry);
scr_ActionHandlerActionEntrySetValues(entry,actionScript,parameterMap,actionHandler,actionName);
ds_list_add(actionHandler.actionEntries,entry);
if(actionHandler.entryIndex==INITIAL_VALUE_NUM)
{
    actionHandler.entryIndex = 0;
}
ds_list_add(actionHandler.entryOrder,entry.entryIndex);

ds_list_add(actionHandler.completedEntries, entry);
show_debug_message("Action++ Completed count: "+string(ds_list_size(actionHandler.completedEntries)));
show_debug_message("Action entry added: "+actionName);

actionHandler.entryIndex++;
actionHandler.entryCount++;
