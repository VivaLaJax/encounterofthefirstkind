var menuContainer = argument0;

if(menuContainer.currentMenuIndex!=0)
{
    var menuStackCount = ds_list_size(menuContainer.menuStack);
    scr_MenuSetHidden(menuContainer.menuStack[| menuStackCount-1],true);
    ds_list_delete(menuContainer.menuStack, menuStackCount-1);
    menuContainer.currentMenuIndex = menuContainer.currentMenuIndex-1;
}
