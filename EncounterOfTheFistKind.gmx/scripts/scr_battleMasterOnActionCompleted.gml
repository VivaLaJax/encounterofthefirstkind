var battleMaster = instance_find(obj_battleMaster,0);
var action = argument0;

if(scr_battleMasterPlayerHasWon())
{
    (instance_find(obj_GameMaster,0)).won = true;
    room_goto(rm_EndScreen);
    return 0;
}

if(scr_battleMasterPlayerHasLost())
{
    (instance_find(obj_GameMaster,0)).lost = true;
    room_goto(rm_EndScreen);
    return 0;
}

ds_list_delete(battleMaster.turnOrder,0);
var turnOrderCount = ds_list_size(battleMaster.turnOrder);
if(turnOrderCount==0)
{
    battleMaster.turnOrder = scr_battleMasterCalculateNextRound(battleMaster.playerParty, battleMaster.enemyCharacters);
    battleMaster.roundNumber = battleMaster.roundNumber+1;
}

scr_MenuContainerOnMenuItemActioned();
scr_battleMasterStartNextTurn(battleMaster);
