var battleGraph = instance_find(obj_battleGraph,0);
var source = argument0;
var range = argument1;

var nodesWithinRange = ds_list_create();
if(range==0)
{
    return nodesWithinRange;
}

if(!ds_map_exists(battleGraph.connectionsMap,source))
{
    return nodesWithinRange;
}
var connectedNodesFromSource = battleGraph.connectionsMap[? source];
var nodesFromSourceCount = ds_list_size(connectedNodesFromSource);

for(var i=0; i<nodesFromSourceCount; i++)
{
    var foundNode = connectedNodesFromSource[| i];
    if(foundNode.objectContained==0)
    {
        ds_list_add(nodesWithinRange, foundNode);
        var nodesFromNext = scr_battleGraphGetNodesWithinRange(foundNode,range-1);
        var nextCount = ds_list_size(nodesFromNext);
        for(var j=0; j<nextCount; j++)
        {
            ds_list_add(nodesWithinRange, nodesFromNext[| j]);
        }
    }   
}

return nodesWithinRange;
