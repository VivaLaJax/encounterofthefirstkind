var soundMaster = instance_find(obj_SoundMaster,0);

soundMaster.volume = soundMaster.volume + soundMaster.volumeStep;

if(soundMaster.volume>soundMaster.volumeMax)
{
    soundMaster.volume = 0;
}
