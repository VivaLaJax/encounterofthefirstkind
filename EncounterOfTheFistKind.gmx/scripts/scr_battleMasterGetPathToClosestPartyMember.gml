var battleMaster = instance_find(obj_battleMaster,0);
var sourceNode = argument0;

var partyNumber = ds_list_size(battleMaster.playerParty);
var closestNode = 0;
var closestDistance = 100;
for(var i=0; i< partyNumber; i++)
{
    if(battleMaster.playerParty[| i].isDead)
    {
        continue;
    }
    var nodeB = scr_battleGraphGetGraphNodeByObject(battleMaster.playerParty[| i]);
    if(nodeB==-1 )
    {
        continue;
    }
    var distance = scr_battleGraphNodeDistance(sourceNode, nodeB,ds_list_create());
    if(distance!=-1 && distance<closestDistance)
    {
        closestNode = nodeB;
    }
}

return scr_battleGraphShortestPathBetweenNodes(sourceNode,nodeB,ds_list_create(),ds_list_create());
