var actionHandler = argument0;

show_debug_message("Action Handler entry index: "+string( actionHandler.entryIndex));
show_debug_message("Action Handler current entry index: "+string(actionHandler.currentEntryIndex));
show_debug_message("Action Handler entry count: "+string(actionHandler.entryCount));
scr_DebugPrintList(actionHandler.animationEntries,"ActionHandler:AnimationEntries");
scr_DebugPrintList(actionHandler.actionEntries,"ActionHandler:ActionEntries");
scr_DebugPrintList(actionHandler.entryOrder,"ActionHandler:EntryOrder");
scr_DebugPrintList(actionHandler.entryNames,"ActionHandler:EntryNames");
scr_DebugPrintList(actionHandler.completedEntries,"ActionHandler:CompletedEntries");
show_debug_message("Action Handler ready for entry: "+string(actionHandler.readyForNextEntry));
show_debug_message("Action Handler action source: "+string(actionHandler.actionSource));
