var menu = argument0;
var change = argument1;

menu.menuItems[| menu.currentItemIndex].highlighted = false;

var menuItemCount = ds_list_size(menu.menuItems);
if(menu.currentItemIndex==0 and change<0)
{
    menu.currentItemIndex = menuItemCount-1;
}
else
{
    menu.currentItemIndex = (menu.currentItemIndex + change) % menuItemCount;
}

menu.menuItems[| menu.currentItemIndex].highlighted = true;
