party = ds_list_create();

var partySize = 1;

for(var i=0; i<partySize; i++)
{
    party[| i] = instance_create(0,0,obj_Character);
    var healthEntry = instance_create(0,0,obj_HealthEntry);
    healthEntry.healthSource = party[| i];
    var healthContainer = instance_find(obj_PlayerHealthContainer,0);
    scr_HealthContainerOnAddEntry(healthContainer, healthEntry);
}

debugInput = true;
