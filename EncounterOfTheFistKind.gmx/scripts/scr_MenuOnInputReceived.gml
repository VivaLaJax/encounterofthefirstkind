var menu = argument0;
var input = argument1;

var actioned = false;

if(input==INPUT_UP or input==INPUT_NW or input==INPUT_NE)
{
    scr_MenuOnItemIndexChanged(menu,-1);
    actioned = true;
}

if(input==INPUT_DOWN or input==INPUT_SW or input==INPUT_SE)
{
    scr_MenuOnItemIndexChanged(menu,1);
    actioned = true;
}

if(input==INPUT_ENTER)
{
    scr_MenuOnItemSelected(menu);
    actioned = true;
}

return actioned;
