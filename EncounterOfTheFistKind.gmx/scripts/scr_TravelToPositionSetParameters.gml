var travelObj = argument0;
var source = argument1[? "source"];
var targetX = argument1[? "targetX"];
var targetY = argument1[? "targetY"];
var timeToTravel = argument1[? "time"];
var sfx = noone;
if(ds_map_exists(argument1,"sfx"))
{
    sfx = argument1[? "sfx"];
}
var actionEntry = argument2;

travelObj.source = source;
travelObj.origX = source.x;
travelObj.origY = source.y;
travelObj.targetX = targetX;
travelObj.targetY = targetY;
travelObj.timeToTravel = timeToTravel;
travelObj.actionEntry = actionEntry;
travelObj.sfx = sfx;
