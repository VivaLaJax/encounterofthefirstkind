# Encounter of The First Kind #
*A personal game project written by Edmund Lewry 2016*
![EncounterScreen.jpg](https://bitbucket.org/repo/gkkLKda/images/381713213-EncounterScreen.jpg)

Written using Gamemaker and it's in built scripting language, GML, this project was my attempt to formalize more of my personal project work. Producing a small scope design and working on elements using a Trello board until the prototype had the features I had intended. While the demo is small and contains some bugs, I produced everything myself; the code, the art and animation, and the music. The systems in place are extendable and relatively robust. I was happy with the work that I did on this and would be able to formalize it into something larger if I wanted to.

Maps are connected automatically based on the ordering of the points placed onto the game level. So the complexity of maps is easily extendable simply by placing more nodes into the level.

### How do I get set up? ###

A playable build is available [here](https://www.dropbox.com/s/k5xr5on0ntd4dgm/EncounterOfTheFistKind.exe?dl=0)!

## Controls

* WASD - For menu movement and target selection
* J - Confirm
* K - Cancel

##Key Known Issues

The biggest issue is that there's a memory leak in the game which I didn't fix before putting the project down.